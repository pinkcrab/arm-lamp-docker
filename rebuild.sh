#!/bin/bash

echo 'Rebuilding the container.......'

docker-compose build --no-cache

echo 'Rebuilt......'

echo 'Starting container for localhost / 127.0.0.1'

docker-compose up --force-recreate

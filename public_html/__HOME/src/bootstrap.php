<?php

declare(strict_types=1);

namespace PinkCrab\Lamp;

use PinkCrab\Lamp\Controllers\HomeController;
use Mustache_Engine, Mustache_Loader_FilesystemLoader;

$mustache = new Mustache_Engine(
	array(
		'loader' => new Mustache_Loader_FilesystemLoader( PC_BASE_PATH . '/views' ),
	)
);


/**
 * God level router
 */
$request = \array_keys( $_GET );

// Fallback to home
if ( empty( $request ) ) {
	$request[] = 'home';
}

switch ( \strval( $requst[0] ) ) {

    // Fallback currently home
	default:
		$render = array(
			new HomeController( $mustache ),
			'home',
		);
		break;

}
\call_user_func( $render );

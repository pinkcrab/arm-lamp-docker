<?php

declare(strict_types=1);

namespace PinkCrab\Lamp\Controllers;

use mysqli;

class HomeController extends AbstractController {

	public function home() {
		$this->setHomeData();
		echo $this->view->render( 'home', $this->data );
	}

	protected function setHomeData() {
		$mysqlConnection = $this->mysqlConnection();
		$this->pushViewData( 'pma_uri', PMA_URI );
		$this->pushViewData( 'php_version', phpversion() );
		$this->pushViewData( 'mysql_version', mysqli_get_server_info( $mysqlConnection ) );
		$this->pushViewData( 'mysql_client', mysqli_get_client_info( $mysqlConnection ) );
		$this->pushViewData( 'sites', join(array_map($this->mapSiteList(), $this->getSites())));
	}

	private function mysqlConnection() {
		$host = 'mariadb';
		$user = 'dbuser';
		$pass = 'password';
		return  new mysqli( $host, $user, $pass );
	}

	public function getSites(): array
	{
		$dir = dirname(PC_BASE_PATH,1);
		$list = [];

		// Open a known directory, and proceed to read its contents
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if (is_dir($dir . '/' . $file) && ! in_array($file, ['logs','__HOME', '.', '..'])) {
						$list[] = $file;
					}
				}
				closedir($dh);
			}
		}
		return $list;
	}

	public function mapSiteList()
	{
		$urlBase = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		return function($site) use ($urlBase){

			return sprintf(
				"<li><a href='%s'>%s</a>",
				$urlBase . $site,
				$site
			);
		};
	}
}

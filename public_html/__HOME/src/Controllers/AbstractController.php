<?php

declare(strict_types=1);

namespace PinkCrab\Lamp\Controllers;

use Mustache_Engine;

abstract class AbstractController {

	protected $view;

	protected $data = array();

	public function __construct( Mustache_Engine $view ) {
		$this->view = $view;

		$this->pushViewData( 'assets_path', PC_BASE_PATH . '/assets/' );
		$this->pushViewData( 'assets_uri', 'http://localhost/__HOME/assets/' );
		$this->pushViewData(
			'image',
			function() {
				return function( string $filename ): string {
					return "http://localhost/__HOME/assets/{$filename}";
				};
			}
		);
	}

	/**
	 * Pushes a key => value set of data to the view collection.
	 *
	 * @param string $key
	 * @param mixed $data
	 * @param bool $overwrite (default FALSE)
	 * @return self
	 */
	public function pushViewData( string $key, $data, bool $overwrite = false ): self {
		if ( $overwrite || ! $this->hasData( $key ) ) {
			$this->data[ $key ] = $data;
		}
		return $this;
	}

	/**
	 * Checks if an array key exists.
	 *
	 * @param string $key
	 * @return bool
	 */
	public function hasData( string $key ): bool {
		return \array_key_exists( $key, $this->data );
	}


}

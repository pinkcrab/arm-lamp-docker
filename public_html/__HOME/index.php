<?php

use PinkCrab\Lamp\Controllers\HomeController;

define( 'PC_BASE_PATH', dirname( __FILE__ ) );
define( 'PMA_URI', 'http://localhost:8080/phpmyadmin/' );

require __DIR__ . '/vendor/autoload.php';
require PC_BASE_PATH . '/src/bootstrap.php';

